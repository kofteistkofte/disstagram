# Disstagram
Disstagram is a simple Discord bot written in Go with [discordgo](https://github.com/bwmarrin/discordgo) library that turns Instagram urls into a url that Discord can produce previews.

## How to run Disstagram

To install disstagram, you need golang installed in your system or server, added to your PATH, and just run this command:
```
go get gitlab.com/kofteistkofte/disstagram
```

After installing, you need to provide a token for your instance with `-t` argument:
```
disstagram -t <your_auth_token>
```
