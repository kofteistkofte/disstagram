package main

import (
	"bytes"
	"flag"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"os/signal"
	"regexp"
	"strings"
	"syscall"

	"github.com/bwmarrin/discordgo"
)

func init() {
	flag.StringVar(&token, "t", "", "Bot Token")
	flag.Parse()
}

var (
	token  string
	buffer = make([][]byte, 0)
)

func main() {
	if token == "" {
		fmt.Println("No token provided")
		return
	}

	dg, err := discordgo.New("Bot " + token)
	if err != nil {
		fmt.Println("An error happened during creating a Discord session: ", err)
		return
	}

	dg.AddHandler(message_create)

	err = dg.Open()
	if err != nil {
		fmt.Println("error while opening connection: ", err)
		return
	}

	fmt.Println("I'm running master, leave everything to me!!!")

	sc := make(chan os.Signal, 1)
	signal.Notify(sc, syscall.SIGINT, syscall.SIGTERM, os.Interrupt, os.Kill)
	<-sc

	dg.Close()
}

func message_create(s *discordgo.Session, m *discordgo.MessageCreate) {
	if m.Author.ID == s.State.User.ID {
		return
	}

	inst_url := find_instagram_url(m.Content)

	if inst_url == "" {
		return
	} else {
		response, err := http.Get(inst_url)
		if err != nil {
			fmt.Println("There is a problem during getting request.", err)
		}
		defer response.Body.Close()

		file, _ := ioutil.ReadAll(response.Body)
		img := bytes.NewReader(file)
		url_parse := strings.Split(inst_url, "/")
		s.ChannelFileSend(m.ChannelID, url_parse[len(url_parse)-3]+".jpg", img)
	}
}

func find_instagram_url(message string) (url string) {
	re := regexp.MustCompile(`(https?:\/\/www\.)?(?:instagram.com|instagr.am)\/p\/([^\/]*)\/?`)
	orj_url := re.Find([]byte(message))
	if string(orj_url) != "" {
		url = string(orj_url) + "media/?size=l"
	}
	return
}
